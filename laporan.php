<?php
ini_set("pcre.backtrack_limit", "5000000");
date_default_timezone_set("Asia/Jakarta");
require_once __DIR__ . '/vendor/autoload.php';

if (!isset($_POST['konsesi_name']) || !isset($_POST['data_type'])) {
	header('location:index.html');
}

//$img = $_POST['img'];
$konsesi_name = $_POST['konsesi_name'];
$data_type = json_decode($_POST['data_type']);
// $data_area = json_decode($data_type[0]->type_data);

// echo "<pre>";print_r($data_area);
// echo "<pre>";print_r($data_type);die;

if (empty($konsesi_name) || empty($data_type)) {
	header('location:index.html');
}

$type_data['rencana_hutan_tanam'] = [
	'rgb(204, 204, 204)' => 'No Data / Awan',
	'rgb(190, 232, 255)' => 'Badan Air',
	'rgb(76, 230, 0)' => 'Dapat Dikembangkan',
	'rgb(230, 0, 0)' => 'Dilindungi',
	'rgb(255, 170, 0)' => 'Lahan Terbangun',
	'rgb(255, 255, 0)' => 'Pengembangan Terbatas'
];

$type_data['emisi'] = [
	'rgb(204, 153, 255)' => 'Tidak Ada',
	'rgb(204, 102, 255)' => 'Deforestasi',
	'rgb(204, 51, 255)' => 'Degradasi'
];

$type_data['stok_karbon'] = [
	'rgb(255, 255, 190)' => '< 25 Ton/Ha',
	'rgb(255, 255, 0)' => '25 - 50 Ton/Ha',
	'rgb(220, 220, 0)' => '50 - 75 Ton/Ha',
	'rgb(168, 168, 0)' => '75 - 100 Ton/Ha',
	'rgb(120, 150, 0)' => '> 100 Ton/Ha'
];

$type_data['lahan_kritis'] = [
	'rgb(190, 232, 255)' => 'Badan Air',
	'rgb(255, 85, 0)' => 'Lahan Terbangun',
	'rgb(79, 0, 0)' => 'Tidak Kritis',
	'rgb(168, 81, 0)' => 'Potensial Kritis',
	'rgb(204, 136, 102)' => 'Agak Kritis',
	'rgb(245, 202, 122)' => 'Kritis',
	'rgb(255, 255, 190)' => 'Sangat Kritis'
];

$type_data['kerapatan_vegetasi'] = [
	'rgb(204, 204, 204)' => 'No Data',
	'rgb(217, 255, 140)' => 'Vegetasi Sangat Rendah',
	'rgb(187, 255, 51)' => 'Vegetasi Rendah',
	'rgb(152, 230, 0)' => 'Vegetasi Sedang',
	'rgb(76, 230, 0)' => 'Vegetasi Rapat',
	'rgb(38, 135, 0)' => 'Vegetasi Sangat Rapat'
];

$type_data['tutupan_lahan'] = [
	'rgb(1, 1, 255)' => 'Badan Air',
	'rgb(111, 199, 155)' => 'Belukar',
	'rgb(85, 144, 119)' => 'Belukar Rawa',
	'rgb(10, 75, 20)' => 'Hutan Lahan Kering Primer',
	'rgb(14, 155, 30)' => 'Hutan Lahan Kering Sekunder',
	'rgb(155, 27, 131)' => 'Hutan Mangrove Primer',
	'rgb(191, 25, 198)' => 'Hutan Mangrove Sekunder',
	'rgb(66, 221, 179)' => 'Hutan Rawa Sekunder',
	'rgb(147, 232, 49)' => 'Hutan Tanaman',
	'rgb(252, 176, 7)' => 'Lahan Terbuka',
	'rgb(0, 128, 128)' => 'Padang Rumput',
	'rgb(255, 255, 0)' => 'Perkebunan',
	'rgb(255, 0, 0)' => 'Permukiman Dan Bangunan Lain',
	'rgb(0, 0, 0)' => 'Pertambangan',
	'rgb(255, 245, 1)' => 'Pertanian Lahan Kering',
	'rgb(228, 242, 118)' => 'Pertanian Lahan Kering Campur',
	'rgb(56, 104, 129)' => 'Rawa',
	'rgb(71, 118, 78)' => 'Sawah',
	'rgb(116, 116, 239)' => 'Tambak',
	'rgb(255, 69, 0)' => 'Transmigrasi'
];

$type_data['tutupan_lahan_2000'] = [
	'rgb(1, 1, 255)' => 'Badan Air',
	'rgb(111, 199, 155)' => 'Belukar',
	'rgb(85, 144, 119)' => 'Belukar Rawa',
	'rgb(10, 75, 20)' => 'Hutan Lahan Kering Primer',
	'rgb(14, 155, 30)' => 'Hutan Lahan Kering Sekunder',
	'rgb(155, 27, 131)' => 'Hutan Mangrove Primer',
	'rgb(191, 25, 198)' => 'Hutan Mangrove Sekunder',
	'rgb(66, 221, 179)' => 'Hutan Rawa Sekunder',
	'rgb(147, 232, 49)' => 'Hutan Tanaman',
	'rgb(252, 176, 7)' => 'Lahan Terbuka',
	'rgb(0, 128, 128)' => 'Padang Rumput',
	'rgb(255, 255, 0)' => 'Perkebunan',
	'rgb(255, 0, 0)' => 'Permukiman Dan Bangunan Lain',
	'rgb(0, 0, 0)' => 'Pertambangan',
	'rgb(255, 245, 1)' => 'Pertanian Lahan Kering',
	'rgb(228, 242, 118)' => 'Pertanian Lahan Kering Campur',
	'rgb(56, 104, 129)' => 'Rawa',
	'rgb(71, 118, 78)' => 'Sawah',
	'rgb(116, 116, 239)' => 'Tambak',
	'rgb(255, 69, 0)' => 'Transmigrasi'
];

$type_data['tutupan_lahan_2019'] = [
	'rgb(1, 1, 255)' => 'Badan Air',
	'rgb(111, 199, 155)' => 'Belukar',
	'rgb(85, 144, 119)' => 'Belukar Rawa',
	'rgb(10, 75, 20)' => 'Hutan Lahan Kering Primer',
	'rgb(14, 155, 30)' => 'Hutan Lahan Kering Sekunder',
	'rgb(155, 27, 131)' => 'Hutan Mangrove Primer',
	'rgb(191, 25, 198)' => 'Hutan Mangrove Sekunder',
	'rgb(66, 221, 179)' => 'Hutan Rawa Sekunder',
	'rgb(147, 232, 49)' => 'Hutan Tanaman',
	'rgb(252, 176, 7)' => 'Lahan Terbuka',
	'rgb(0, 128, 128)' => 'Padang Rumput',
	'rgb(255, 255, 0)' => 'Perkebunan',
	'rgb(255, 0, 0)' => 'Permukiman Dan Bangunan Lain',
	'rgb(0, 0, 0)' => 'Pertambangan',
	'rgb(255, 245, 1)' => 'Pertanian Lahan Kering',
	'rgb(228, 242, 118)' => 'Pertanian Lahan Kering Campur',
	'rgb(56, 104, 129)' => 'Rawa',
	'rgb(71, 118, 78)' => 'Sawah',
	'rgb(116, 116, 239)' => 'Tambak',
	'rgb(255, 69, 0)' => 'Transmigrasi'
];
function data($type_data, $type, $data_area) {
	$data = '';
	$luas_total = 0;
	$emisi_total = 0;
	foreach ($type_data[$type] as $key => $value) {
		$luas = 0;
		$emisi = 0;
		foreach ($data_area as $k => $v) {
			if ($v->kelas == $value) {
				$emisi += ($v->luas*$v->se);
				/* if ($emisi == 0) {
					$luas += 0;
				} else { */
					$luas += $v->luas;
				//}
			}
		}
		//if ($luas != 0) {
			$luas_total += $luas;
			$emisi_total += $emisi;
			$kolom_emisi = '';
			if ($type=='emisi') {
				$kolom_emisi ='<td>'.$emisi.'</td>';
			}
			$data .= '
						<tr>
							<td style="font-size: 12px;" align="center">
								<span style="background-color: '.$key.';width: 50px!important;height: 20px;">
								&nbsp;
								&nbsp;
								&nbsp;
								</span>
							</td>
							<td>
								'.$value.'
							</td>
							'.$kolom_emisi.'
							<td>
								'.$luas.'
							</td>
						</tr>
					';
		//}
	}
	if ($type=='emisi') {
		$emisiTotal = '<td style="font-weight: bold;">'.$emisi_total.'</td>';
	} else {
		$emisiTotal = '';
	}
	$data .= '
			<tr>
				<td colspan="2" align="center" style="font-weight: bold;">
					Total :
				</td>
				'.$emisiTotal.'
				<td style="font-weight: bold;">
					'.$luas_total.'
				</td>
			</tr>
		';
	return $data;
}
$data2 = '';
foreach ($data_type as $key => $value) {
$data_area = json_decode($value->type_data);
//print_r($data_area);exit;
if($value->type_name=='emisi'){
	$kolom1 = 'Aktifitas';
	$kolom2 = '<td align="center" style="font-weight: bold;">Total Emisi (tCO2e)</td>';
	$kolom3 = 'Luas (Ha)';
	$jml_kolom = 4;
}else{
	$kolom1 = 'Keterangan';
	$kolom2 = '';
	$kolom3 = 'Luas (Ha)';
	$jml_kolom = 3;
}
$data2 .= '
<tr style="box-decoration-break: clone;">
<td colspan="2" style="text-align: center;">
	<br><br><br>
	<img src="'.$value->type_image.'" width="100%" height="60%"/>
</td>
</tr>
<tr>
<td>
	<table border="1" style="border-collapse: collapsed;width: 100%;">
		<tr>
			<td colspan="'.$jml_kolom.'">
				<h2>'.ucwords(str_replace("_", " ", $value->type_name)).'</h2>
			</td>
		</tr>
		<tr style="border-bottom:1px solid rgba(0,0,0,1);">
			<td></td>
			<td align="center" style="font-weight: bold;">'.$kolom1.'</td>
			'.$kolom2.'
			<td align="center" style="font-weight: bold;">'.$kolom3.'</td>
		</tr>
		'.data($type_data, $value->type_name, $data_area).'
	</table>
</td>
</tr>';
}
//print_r($data2);die;
$mpdf = new \Mpdf\Mpdf();
$mpdf->WriteHTML('<style>td {padding: 5px;}</style><table>
<tr>
	<td colspan="2" style="text-align: center;">
		<h1>'.$konsesi_name.'</h1>
	</td>
</tr>
'.$data2.'
</table>');
//$mpdf->Output();
$date = date("d-m-Y_H-i-s");
$mpdf->Output("Laporan_{$date}.pdf", 'I');

?>