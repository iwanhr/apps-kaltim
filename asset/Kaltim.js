		var shpfile = "";
		var crop = "";
		var crop_layer = "";
		var crop_data_layer = [];
		var data_konsesi = [];
		var index_data = 0
		$("#jstree").jstree({
			plugins: ["wholerow", "checkbox"],
			core: {
				data: [{
					text: "Landsat 2000",
					children: [{
						text: "Landsat 2000 - 1",
						icon: "//jstree.com/tree-icon.png"
					},
					{
						text: "Landsat 2000 - 2",
						icon: "//jstree.com/tree-icon.png"
					},
					{
						text: "Landsat 2000 - 3",
						icon: "//jstree.com/tree-icon.png"
					},
					{
						text: "Landsat 2000 - 4",
						icon: "//jstree.com/tree-icon.png"
					},
					{
						text: "Landsat 2000 - 5",
						icon: "//jstree.com/tree-icon.png"
					},
					{
						text: "Landsat 2000 - 6",
						icon: "//jstree.com/tree-icon.png"
					},
					{
						text: "Landsat 2000 - 7",
						icon: "//jstree.com/tree-icon.png"
					},
					{
						text: "Landsat 2000 - 8",
						icon: "//jstree.com/tree-icon.png"
					},
					{
						text: "Landsat 2000 - 9",
						icon: "//jstree.com/tree-icon.png"
					},
					{
						text: "Landsat 2000 - 10",
						icon: "//jstree.com/tree-icon.png"
					},
					{
						text: "Landsat 2000 - 11",
						icon: "//jstree.com/tree-icon.png"
					},
					{
						text: "Landsat 2000 - 12",
						icon: "//jstree.com/tree-icon.png"
					},
					{
						text: "Landsat 2000 - 13",
						icon: "//jstree.com/tree-icon.png"
					}
					],
					state: {
						opened: true
					}
				},
				{
					text: "Landsat 2019",
					children: [{
						text: "Landsat 2019 - 1",
						icon: "//jstree.com/tree-icon.png"
					},
					{
						text: "Landsat 2019 - 2",
						icon: "//jstree.com/tree-icon.png"
					},
					{
						text: "Landsat 2019 - 3",
						icon: "//jstree.com/tree-icon.png"
					},
					{
						text: "Landsat 2019 - 4",
						icon: "//jstree.com/tree-icon.png"
					},
					{
						text: "Landsat 2019 - 5",
						icon: "//jstree.com/tree-icon.png"
					},
					{
						text: "Landsat 2019 - 6",
						icon: "//jstree.com/tree-icon.png"
					},
					{
						text: "Landsat 2019 - 7",
						icon: "//jstree.com/tree-icon.png"
					},
					{
						text: "Landsat 2019 - 8",
						icon: "//jstree.com/tree-icon.png"
					},
					{
						text: "Landsat 2019 - 9",
						icon: "//jstree.com/tree-icon.png"
					},
					{
						text: "Landsat 2019 - 10",
						icon: "//jstree.com/tree-icon.png"
					},
					{
						text: "Landsat 2019 - 11",
						icon: "//jstree.com/tree-icon.png"
					},
					{
						text: "Landsat 2019 - 12",
						icon: "//jstree.com/tree-icon.png"
					},
					{
						text: "Landsat 2019 - 13",
						icon: "//jstree.com/tree-icon.png"
					}
					],
					state: {
						opened: true
					}
				}
				]
			}
		});
		$("#jstree").on("changed.jstree", function (e, data) {
			if (data.action == "select_node") {
				if (data.selected != "") {
					data.selected.forEach(function (dt) {
						if (dt != "j1_1" || dt != "j1_15") {
							add_landsat("show", lndst[dt], dt);
						}
					});
				}
			}

			if (data.action == "deselect_node") {
				if (data.node.id != "") {
					if (data.node.id == "j1_1") {
						var dt_1 = [
							"j1_2",
							"j1_3",
							"j1_4",
							"j1_5",
							"j1_6",
							"j1_7",
							"j1_8",
							"j1_9",
							"j1_10",
							"j1_11",
							"j1_12",
							"j1_13",
							"j1_14"
						];
						dt_1.forEach(function (dt) {
							add_landsat("hide", null, dt);
						});
					} else if (data.node.id == "j1_15") {
						var dt_1 = [
							"j1_16",
							"j1_17",
							"j1_18",
							"j1_19",
							"j1_20",
							"j1_21",
							"j1_22",
							"j1_23",
							"j1_24",
							"j1_25",
							"j1_26",
							"j1_27",
							"j1_28"
						];
						dt_1.forEach(function (dt) {
							add_landsat("hide", null, dt);
						});
					} else {
						add_landsat("hide", null, data.node.id);
					}
				}
			}
		});

		var lndst = {
			j1_2: "KML_2000/L_2000_1/L_2000_1.kml",
			j1_3: "KML_2000/L_2000_2/L_2000_2.kml",
			j1_4: "KML_2000/L_2000_3/L_2000_3.kml",
			j1_5: "KML_2000/L_2000_4/L_2000_4.kml",
			j1_6: "KML_2000/L_2000_5/L_2000_5.kml",
			j1_7: "KML_2000/L_2000_6/L_2000_6.kml",
			j1_8: "KML_2000/L_2000_7/L_2000_7.kml",
			j1_9: "KML_2000/L_2000_8/L_2000_8.kml",
			j1_10: "KML_2000/L_2000_9/L_2000_9.kml",
			j1_11: "KML_2000/L_2000_10/L_2000_10.kml",
			j1_12: "KML_2000/L_2000_11/L_2000_11.kml",
			j1_13: "KML_2000/L_2000_12/L_2000_12.kml",
			j1_14: "KML_2000/L_2000_13/L_2000_13.kml",
			j1_16: "KML_2019/L_2019_1/L_2019_1.kml",
			j1_17: "KML_2019/L_2019_2/L_2019_2.kml",
			j1_18: "KML_2019/L_2019_3/L_2019_3.kml",
			j1_19: "KML_2019/L_2019_4/L_2019_4.kml",
			j1_20: "KML_2019/L_2019_5/L_2019_5.kml",
			j1_21: "KML_2019/L_2019_6/L_2019_6.kml",
			j1_22: "KML_2019/L_2019_7/L_2019_7.kml",
			j1_23: "KML_2019/L_2019_8/L_2019_8.kml",
			j1_24: "KML_2019/L_2019_9/L_2019_9.kml",
			j1_25: "KML_2019/L_2019_10/L_2019_10.kml",
			j1_26: "KML_2019/L_2019_11/L_2019_11.kml",
			j1_27: "KML_2019/L_2019_12/L_2019_12.kml",
			j1_28: "KML_2019/L_2019_13/L_2019_13.kml"
		};

		var landsat_2000_1,
			landsat_2000_2,
			landsat_2000_3,
			landsat_2000_4,
			landsat_2000_5,
			landsat_2000_6,
			landsat_2000_7,
			landsat_2000_8,
			landsat_2000_9,
			landsat_2000_10,
			landsat_2000_11,
			landsat_2000_12,
			landsat_2000_13,
			landsat_2019_1,
			landsat_2019_2,
			landsat_2019_3,
			landsat_2019_4,
			landsat_2019_5,
			landsat_2019_6,
			landsat_2019_7,
			landsat_2019_8,
			landsat_2019_9,
			landsat_2019_10,
			landsat_2019_11,
			landsat_2019_12,
			landsat_2019_13;

		var landsat_data = [];

		var j1_2,
			j1_3,
			j1_4,
			j1_5,
			j1_6,
			j1_7,
			j1_8,
			j1_9,
			j1_10,
			j1_11,
			j1_12,
			j1_13,
			j1_14,
			j1_16,
			j1_17,
			j1_18,
			j1_19,
			j1_20,
			j1_21,
			j1_22,
			j1_23,
			j1_24,
			j1_25,
			j1_26,
			j1_27,
			j1_28;

		function add_landsat(state, source, variable) {
			if (state == "show" && !map.hasLayer(landsat_data[variable])) {
				$(".loader").show();
				fetch(source)
					.then(res => res.text())
					.then(kmltext => {
						// Create new kml overlay
						const parser = new DOMParser();
						const kml = parser.parseFromString(kmltext, "text/xml");
						landsat_data[variable] = new L.KML(kml);
						map.addLayer(landsat_data[variable]);

						// Adjust map to show the kml
						if (landsat_data[variable].getBounds()) {
							const bounds = landsat_data[variable].getBounds();
							map.fitBounds(bounds);
						}

						if (map.hasLayer(shpfile)) {
							shpfile.bringToFront();
						}

						$(".loader").hide();
					});
			}
			if (state == "hide" && map.hasLayer(landsat_data[variable])) {
				$(".loader").show();
				map.removeLayer(landsat_data[variable]);
				$(".loader").hide();
			}
		}

		$("#landsat").hide();
		$("#upload_konsesi").hide();
		$("#toggle_menu").click(function () {
			$("#menu_mobile").toggle();
		});

		function btn_close(element) {
			$(element)
				.parent()
				.parent()
				.hide();
		}

		var map = L.map("map", {
			center: [0.7893, 113.9213],
			zoom: 4
		});
		L.control.scale().addTo(map);
		L.tileLayer("https://mt0.google.com/vt/lyrs=y&hl=en&x={x}&y={y}&z={z}", {
			maxZoom: 18
		}).addTo(map);

		function Kerapatan_Vegetasi_Style(feature, layer) {
			//console.log(feature.properties['Kerapatan'])
			switch (feature.properties["Vegetasi"]) {
				case "No Data":
					return {
						color: "rgb(204, 204, 204)",
						weight: 1
					};
				case "Vegetasi Sangat Rendah":
					return {
						color: "rgb(217, 255, 140)",
						weight: 1
					};
				case "Vegetasi Rendah":
					return {
						color: "rgb(187, 255, 51)",
						weight: 1
					};
				case "Vegetasi Sedang":
					return {
						color: "rgb(152, 230, 0)",
						weight: 1
					};
				case "Vegetasi Rapat":
					return {
						color: "rgb(76, 230, 0)",
						weight: 1
					};
				case "Vegetasi Sangat Rapat":
					return {
						color: "rgb(38, 135, 0)",
						weight: 1
					};
			}
		}

		function Lahan_Kritis_Style(feature, layer) {
			//console.log(feature.properties['Kelas'])
			switch (feature.properties["Kelas"]) {
				case "Badan Air":
					return {
						color: "rgb(190, 232, 255)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Lahan Terbangun":
					return {
						color: "rgb(255, 85, 0)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Tidak Kritis":
					return {
						color: "rgb(79, 0, 0)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Potensial Kritis":
					return {
						color: "rgb(168, 81, 0)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Agak Kritis":
					return {
						color: "rgb(204, 136, 102)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Kritis":
					return {
						color: "rgb(245, 202, 122)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Sangat Kritis":
					return {
						color: "rgb(255, 255, 190)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
			}
		}

		function Tutupan_Lahan_Style(feature, layer) {
			//console.log(feature.properties['Kelas'])
			switch (feature.properties["Kelas"]) {
				case "Badan Air":
					return {
						color: "rgb(1, 1, 255)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Belukar":
					return {
						color: "rgb(111, 199, 155)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Belukar Rawa":
					return {
						color: "rgb(85, 144, 119)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Hutan Lahan Kering Primer":
					return {
						color: "rgb(10, 75, 20)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Hutan Lahan Kering Sekunder":
					return {
						color: "rgb(14, 155, 30)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Hutan Mangrove Primer":
					return {
						color: "rgb(155, 27, 131)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Hutan Mangrove Sekunder":
					return {
						color: "rgb(191, 25, 198)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Hutan Rawa Sekunder":
					return {
						color: "rgb(66, 221, 179)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Hutan Tanaman":
					return {
						color: "rgb(147, 232, 49)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Lahan Terbuka":
					return {
						color: "rgb(252, 176, 7)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Padang Rumput":
					return {
						color: "rgb(0, 128, 128)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Perkebunan":
					return {
						color: "rgb(255, 255, 0)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Permukiman Dan Bangunan Lain":
					return {
						color: "rgb(255, 0, 0)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Pertambangan":
					return {
						color: "rgb(0, 0, 0)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Pertanian Lahan Kering":
					return {
						color: "rgb(255, 245, 1)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Pertanian Lahan Kering Campur":
					return {
						color: "rgb(228, 242, 118)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Rawa":
					return {
						color: "rgb(56, 104, 129)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Sawah":
					return {
						color: "rgb(71, 118, 78)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Tambak":
					return {
						color: "rgb(116, 116, 239)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Transmigrasi":
					return {
						color: "rgb(255, 69, 0)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Bandara dan Pelabuhan":
					return {
						color: "rgb(27, 27, 33)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
			}
		}

		function Rencana_Hutan_Tanam_Style(feature, layer) {
			//console.log(feature.properties['Area_Hutan'])
			switch (feature.properties["Area_Hutan"]) {
				case "No Data / Awan":
					return {
						color: "rgb(204, 204, 204)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Badan Air":
					return {
						color: "rgb(190, 232, 255)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Dapat Dikembangkan":
					return {
						color: "rgb(76, 230, 0)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Dilindungi":
					return {
						color: "rgb(230, 0, 0)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Lahan Terbangun":
					return {
						color: "rgb(255, 170, 0)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Pengembangan Terbatas":
					return {
						color: "rgb(255, 255, 0)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
			}
		}

		function Stok_Karbon_Style(feature, layer) {
			//console.log(feature.properties['Cadangan_K'])
			switch (feature.properties["Cadangan_K"]) {
				case "< 25 Ton/Ha":
					return {
						color: "rgb(255, 255, 190)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "25 - 50 Ton/Ha":
					return {
						color: "rgb(255, 255, 0)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "50 - 75 Ton/Ha":
					return {
						color: "rgb(220, 220, 0)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "75 - 100 Ton/Ha":
					return {
						color: "rgb(168, 168, 0)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "> 100 Ton/Ha":
					return {
						color: "rgb(120, 150, 0)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
			}
		}

		function Emisi_Style(feature, layer) {
			//console.log(feature.properties['Cadangan_K'])
			switch (feature.properties["Aktifitas"]) {
				case "Tidak Ada":
					return {
						color: "rgb(204, 153, 255)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Deforestasi":
					return {
						color: "rgb(204, 102, 255)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
				case "Degradasi":
					return {
						color: "rgb(204, 51, 255)",
						weight: 1,
						opacity: 0.1,
						dashArray: "3",
						fillOpacity: 1
					};
			}
		}

		var geojson_list = [{
			source: "Provinsi.json",
			name: "Provinsi",
			json: "Provinsi_json",
			style: "Provinsi_style"
		},
		{
			source: "Jalan.geojson",
			name: "Jalan",
			json: "Jalan_json",
			style: "Jalan_style"
		},
		{
			source: "BTSAdmin.json",
			name: "BTSAdmin",
			json: "BTSAdmin_json",
			style: "BTSAdmin_style"
		},
		{
			source: "Kecamatan.json",
			name: "kecamatan",
			json: "kecamatan_json",
			style: "kecamatan_style"
		},
		{
			source: "desa.json",
			name: "desa",
			json: "desa_json",
			style: "desa_style"
		},
		{
			source: "Status_Hutan.geojson",
			name: "ForestStatus",
			json: "ForestStatus_json",
			style: "ForestStatus_style"
		},
		{
			source: "EastKal_timber_plantation_HTI.json",
			name: "EastKal_timber_plantation_HTI",
			json: "EastKal_timber_plantation_HTI_json",
			style: "EastKal_timber_plantation_HTI_style"
		}
			/*{'source' : 'DbD_D.json', 'name' : 'AnalisisDBD', 'json' : 'AnalisisDBD_json', 'style' : 'AnalisisDBD_style'}*/
		];
		var geojson_data = [];

		geojson_list.forEach(function (dt) {
			$(".loader").show()
			geojson_data[dt.json] = new L.GeoJSON.AJAX("json/" + dt.source, {
				style: eval(dt.style),
				onEachFeature: function (feature, layer) {
					if (feature.properties) {
						layer.bindPopup(
							Object.keys(feature.properties)
								.map(function (k) {
									return k + ": " + feature.properties[k];
								})
								.join("<br />"), {
							maxHeight: 200
						}
						);
						$(".loader").hide()
					}
					$(".loader").hide()
				}
			});
		});

		var lk = [];
		var lahan_kritis_ = [];

		function lahan_kritis() {
			if (map.hasLayer(lahan_kritis_)) {
				$('.save_data').hide();
				$('#type').val('');
				$(".lahan_kritis").removeClass("active");
				$(".information_color").hide();
				map.removeLayer(lahan_kritis_);
				for (var i = 1; i <= 13; i++) {
					if (map.hasLayer(lk[i])) {
						map.removeLayer(lk[i]);
					}
				}
				clear_all();
				if (shpfile && !map.hasLayer(shpfile)) {
					map.addLayer(shpfile);
					shpfile.bringToFront();
				}
			} else {
				clear_all();
				if (shpfile && !map.hasLayer(shpfile)) {
					map.addLayer(shpfile);
					shpfile.bringToFront();
				}
				$('.save_data').show();
				$('#type').val('lahan_kritis');
				$(".lahan_kritis").addClass("active");
				$(".information_color").show();
				$(".information_color_title").html(`Lahan Kritis`);
				$(".information_color_data").html(`
<li><span style="background: rgb(190, 232, 255);">&nbsp;</span> <font>Badan Air</font></li>
<li><span style="background: rgb(255, 85, 0);">&nbsp;</span> <font>Lahan Terbangun</font></li>
<li><span style="background: rgb(79, 0, 0);">&nbsp;</span> <font>Tidak Kritis</font></li>
<li><span style="background: rgb(168, 81, 0);">&nbsp;</span> <font>Potensial Kritis</font></li>
<li><span style="background: rgb(204, 136, 102);">&nbsp;</span> <font>Agak Kritis</font></li>
<li><span style="background: rgb(245, 202, 122);">&nbsp;</span> <font>Kritis</font></li>
<li><span style="background: rgb(255, 255, 190);">&nbsp;</span> <font>Sangat Kritis</font></li>
						`);
				var urutan = 1;
				$(".loader").show();
				$.ajax({
					url: "json/Scene_Box.geojson",
					beforeSend: function (xhr) {
						if (xhr.overrideMimeType) {
							xhr.overrideMimeType("application/json");
						}
					},
					dataType: "json",
					data: null,
					success: function (data, textStatus, request) {
						lahan_kritis_ = L.geoJson(data, {
							onEachFeature: function (feature, layer) {
								if (feature.properties) {
									layer.id_lh = feature.properties.Id;
									urutan = urutan + 1;
								}
							},
							style: Jalan_style
						}).addTo(map);
						if (lahan_kritis_.getBounds()) {
							map.fitBounds(lahan_kritis_.getBounds());
						}
						lahan_kritis_.eachLayer(function (layer) {
							layer.on({
								click: function () {
									load_lahan_kritis(layer.id_lh);
								}
							});
						});
						$(".loader").hide();
					}
				});
			}
		}

		function load_lahan_kritis(i) {
			if (!map.hasLayer(shpfile)) {
				alert('Konsesi tidak ditemukan! Silakan unggah konsesi. Jika sudah mengunggah konsesi, silakan centang konsesi di menu sebelah kanan atas.')
			}
			if (!map.hasLayer(lk[i]) && !lk[i]) {
				index_data = 1
				$(".loader").show();
				$.ajax({
					url: "json/LahanKritis_GJson/LahanKritis_" + i + ".geojson",
					beforeSend: function (xhr) {
						if (xhr.overrideMimeType) {
							xhr.overrideMimeType("application/json");
						}
					},
					dataType: "json",
					data: null,
					success: function (data, textStatus, request) {
						lk[i] = L.geoJson(data, {
							onEachFeature: function (feature, layer) {
								crop_konsesi(feature, layer, "Lahan_Kritis", i)
								$("#data_area").val(JSON.stringify(data_konsesi))
								if (feature.properties) {
									layer.bindPopup(
										Object.keys(feature.properties)
											.map(function (k) {
												return k + ": " + feature.properties[k];
											})
											.join("<br />"), {
										maxHeight: 200
									}
									);
								}
							},
							style: Lahan_Kritis_Style
						})/*.addTo(map);*/
						if (map.hasLayer(shpfile)) {
							if (shpfile.getBounds()) {
								map.fitBounds(shpfile.getBounds());
							}
							shpfile.bringToFront();
							map.removeLayer(shpfile);
						}
						/*console.log(index_data)
						for (let index = 1; index <= index_data; index++) {
							try {
								console.log(crop_data_layer[index])
								if (map.hasLayer(crop_data_layer[index])) {
									crop_data_layer[index].bringToFront();
								}
							} catch (err) {
								console.log(err)
							}
						}*/
						$(".loader").hide();
					}
				});
			}
		}

		function clear_all() {
			//var all_layer = [];

			data_konsesi.length = 0
			$("#data_area").val('[]')

			map.eachLayer(function (layer) {
				if (!layer._map) {
					map.removeLayer(layer);
				}
				/* map.removeLayer(layer); */
				//console.log(layer)
			});

			$(".batas_konsesi").removeClass("active");
			$(".input_konsesi").removeClass("active");
			$(".tutupan_lahan").removeClass("active");
			$(".kerapatan_vegetasi").removeClass("active");
			$(".lahan_kritis").removeClass("active");
			$(".stok_karbon").removeClass("active");
			$(".rencana_hutan_tanam").removeClass("active");
			$(".emisi").removeClass("active");

			$(".save_data").hide();
			$(".information_color").hide();
			$(".landsat_select").hide();
			$(".tutupan_lahan_select").hide();
			$(".upload_konsesi").hide();

			if (map.hasLayer(kerapatan_vegetasi_)) {
				map.removeLayer(kerapatan_vegetasi_);
			}
			if (map.hasLayer(stok_karbon_)) {
				map.removeLayer(stok_karbon_);
			}
			if (map.hasLayer(rencana_hutan_tanam_)) {
				map.removeLayer(rencana_hutan_tanam_);
			}
			if (map.hasLayer(landsat_)) {
				map.removeLayer(landsat_);
			}
			if (map.hasLayer(tutupan_lahan_)) {
				map.removeLayer(tutupan_lahan_);
			}
			if (map.hasLayer(lahan_kritis_)) {
				map.removeLayer(lahan_kritis_);
			}
			if (map.hasLayer(emisi_)) {
				map.removeLayer(emisi_);
			}

			for (var i = 1; i <= 13; i++) {
				add_landsat("hide", null, i);
			}

			for (var z = 1; z <= 13; z++) {
				for (var i = 1; i <= index_data; i++) {
					if (map.hasLayer(crop_data_layer[z + "_" + i])) {
						map.removeLayer(crop_data_layer[z + "_" + i]);
					}
				}
			}
			//crop_data_layer = []

			for (var i = 1; i <= 13; i++) {
				if (map.hasLayer(tl[i])) {
					map.removeLayer(tl[i]);
				}
				if (map.hasLayer(kv[i])) {
					map.removeLayer(kv[i]);
				}
				if (map.hasLayer(lk[i])) {
					map.removeLayer(lk[i]);
				}
				if (map.hasLayer(rht[i])) {
					map.removeLayer(rht[i]);
				}
				if (map.hasLayer(sk[i])) {
					map.removeLayer(sk[i]);
				}
				if (map.hasLayer(em[i])) {
					map.removeLayer(em[i]);
				}
			}
			tl = []
			kv = []
			lk = []
			rht = []
			sk = []
			em = []
		}

		function tutupan_lahan_select() {
			if ($(".tutupan_lahan_select").is(":visible")) {
				$('.save_data').hide();
				$('#type').val('');
				$("input[type=radio][name=tutupan_lahan]").prop("checked", false);
				$(".tutupan_lahan").removeClass("active");
				$(".tutupan_lahan_select").hide();
				$(".information_color").hide();
				if (map.hasLayer(tutupan_lahan_)) {
					map.removeLayer(tutupan_lahan_);
				}
				for (var i = 1; i <= 13; i++) {
					if (map.hasLayer(tl[i])) {
						map.removeLayer(tl[i]);
					}
				}
			} else {
				clear_all();
				$("input[type=radio][name=tutupan_lahan]").prop("checked", false);
				$(".tutupan_lahan").addClass("active");
				$(".tutupan_lahan_select").show();
			}
		}

		$("input[type=radio][name=tutupan_lahan]").change(function () {
			if (this.value == "2000") {
				// for (var i = 1; i <= index_data; i++) {
				// if (map.hasLayer(crop_data_layer[i])) {
				// map.removeLayer(crop_data_layer[i]);
				// }
				// }
				for (var z = 1; z <= 13; z++) {
					for (var i = 1; i <= index_data; i++) {
						if (map.hasLayer(crop_data_layer[z + "_" + i])) {
							map.removeLayer(crop_data_layer[z + "_" + i]);
						}
					}
				}
				tutupan_lahan("2000");
				$('#type').val('tutupan_lahan_2000')
			} else if (this.value == "2019") {
				// for (var i = 1; i <= index_data; i++) {
				// if (map.hasLayer(crop_data_layer[i])) {
				// map.removeLayer(crop_data_layer[i]);
				// }
				// }
				for (var z = 1; z <= 13; z++) {
					for (var i = 1; i <= index_data; i++) {
						if (map.hasLayer(crop_data_layer[z + "_" + i])) {
							map.removeLayer(crop_data_layer[z + "_" + i]);
						}
					}
				}
				tutupan_lahan("2019");
				$('#type').val('tutupan_lahan_2019')
			}
		});

		var tl = [];
		var tutupan_lahan_ = [];

		function tutupan_lahan(tahun) {
			data_konsesi.length = 0
			$("#data_area").val('[]')
			$('.save_data').show();
			$(".information_color").hide();
			if (map.hasLayer(tutupan_lahan_)) {
				map.removeLayer(tutupan_lahan_);
			}
			for (var i = 1; i <= 13; i++) {
				if (map.hasLayer(tl[i])) {
					map.removeLayer(tl[i]);
				}
			}
			//clear_all();
			if (shpfile && !map.hasLayer(shpfile)) {
				map.addLayer(shpfile);
				shpfile.bringToFront();
			}
			$(".information_color").show();
			$(".information_color_title").html(`Tutupan Lahan ` + tahun);
			var data_color = `
<li><span style="background: rgb(1, 1, 255);">&nbsp;</span> <font>Badan Air</font></li>
<li><span style="background: rgb(111, 199, 155);">&nbsp;</span> <font>Belukar</font></li>
<li><span style="background: rgb(85, 144, 119);">&nbsp;</span> <font>Belukar Rawa</font></li>
<li><span style="background: rgb(10, 75, 20);">&nbsp;</span> <font>Hutan Lahan Kering Primer</font></li>
<li><span style="background: rgb(14, 155, 30);">&nbsp;</span> <font>Hutan Lahan Kering Sekunder</font></li>
<li><span style="background: rgb(155, 27, 131);">&nbsp;</span> <font>Hutan Mangrove Primer</font></li>
<li><span style="background: rgb(191, 25, 198);">&nbsp;</span> <font>Hutan Mangrove Sekunder</font></li>
<li><span style="background: rgb(66, 221, 179);">&nbsp;</span> <font>Hutan Rawa Sekunder</font></li>
<li><span style="background: rgb(147, 232, 49);">&nbsp;</span> <font>Hutan Tanaman</font></li>
<li><span style="background: rgb(252, 176, 7);">&nbsp;</span> <font>Lahan Terbuka</font></li>
<li><span style="background: rgb(0, 128, 128);">&nbsp;</span> <font>Padang Rumput</font></li>
<li><span style="background: rgb(255, 255, 0);">&nbsp;</span> <font>Perkebunan</font></li>
<li><span style="background: rgb(255, 0, 0);">&nbsp;</span> <font>Permukiman Dan Bangunan Lain</font></li>
<li><span style="background: rgb(0, 0, 0);">&nbsp;</span> <font>Pertambangan</font></li>
<li><span style="background: rgb(255, 245, 1);">&nbsp;</span> <font>Pertanian Lahan Kering</font></li>
<li><span style="background: rgb(228, 242, 118);">&nbsp;</span> <font>Pertanian Lahan Kering Campur</font></li>
<li><span style="background: rgb(56, 104, 129);">&nbsp;</span> <font>Rawa</font></li>
<li><span style="background: rgb(71, 118, 78);">&nbsp;</span> <font>Sawah</font></li>
<li><span style="background: rgb(116, 116, 239);">&nbsp;</span> <font>Tambak</font></li>
<li><span style="background: rgb(255, 69, 0);">&nbsp;</span> <font>Transmigrasi</font></li>
`;
			if (tahun == "2019") {
				data_color +=
					`<li><span style="background: rgb(27, 27, 33);">&nbsp;</span> <font>Bandara dan Pelabuhan</font></li>`;
			}

			$(".information_color_data").html(data_color);
			var urutan = 1;
			$(".loader").show();
			$.ajax({
				url: "json/Scene_Box.geojson",
				beforeSend: function (xhr) {
					if (xhr.overrideMimeType) {
						xhr.overrideMimeType("application/json");
					}
				},
				dataType: "json",
				data: null,
				success: function (data, textStatus, request) {
					tutupan_lahan_ = L.geoJson(data, {
						onEachFeature: function (feature, layer) {
							if (feature.properties) {
								layer.id_lh = feature.properties.Id;
								urutan = urutan + 1;
							}
						},
						style: Jalan_style
					}).addTo(map);
					if (tutupan_lahan_.getBounds()) {
						map.fitBounds(tutupan_lahan_.getBounds());
					}
					tutupan_lahan_.eachLayer(function (layer) {
						layer.on({
							click: function () {
								load_tutupan_lahan(layer.id_lh, tahun);
							}
						});
					});
					$(".loader").hide();
				}
			});
		}

		function load_tutupan_lahan(i, tahun) {
			if (!map.hasLayer(shpfile)) {
				alert('Konsesi tidak ditemukan! Silakan unggah konsesi. Jika sudah mengunggah konsesi, silakan centang konsesi di menu sebelah kanan atas.')
			}
			if (!map.hasLayer(tl[i])) {
				index_data = 1
				$(".loader").show();
				$.ajax({
					url: "json/LC_" + tahun + "_Gjson/LC_" + tahun + "_" + i + ".geojson",
					beforeSend: function (xhr) {
						if (xhr.overrideMimeType) {
							xhr.overrideMimeType("application/json");
						}
					},
					dataType: "json",
					data: null,
					success: function (data, textStatus, request) {
						tl[i] = L.geoJson(data, {
							onEachFeature: function (feature, layer) {
								crop_konsesi(feature, layer, "Tutupan_Lahan", i)
								$("#data_area").val(JSON.stringify(data_konsesi))
								if (feature.properties) {
									layer.bindPopup(
										Object.keys(feature.properties)
											.map(function (k) {
												return k + ": " + feature.properties[k];
											})
											.join("<br />"), {
										maxHeight: 200
									}
									);
								}
							},
							style: Tutupan_Lahan_Style
						})/*.addTo(map);*/
						if (map.hasLayer(shpfile)) {
							if (shpfile.getBounds()) {
								map.fitBounds(shpfile.getBounds());
							}
							shpfile.bringToFront();
							map.removeLayer(shpfile);
						}
						$(".loader").hide();
					}
				});
			}
		}

		function landsat_select() {
			if ($(".landsat_select").is(":visible")) {
				$("input[type=radio][name=landsat]").prop("checked", false);
				$(".input_konsesi").removeClass("active");
				$(".landsat_select").hide();
				$(".information_color").hide();
				if (map.hasLayer(landsat_)) {
					map.removeLayer(landsat_);
				}
				for (var i = 1; i <= 13; i++) {
					add_landsat("hide", null, i);
				}
			} else {
				clear_all();
				$("input[type=radio][name=landsat]").prop("checked", false);
				$(".input_konsesi").addClass("active");
				$(".landsat_select").show();
			}
		}

		$("input[type=radio][name=landsat]").change(function () {
			if (this.value == "2000") {
				landsat("2000");
			} else if (this.value == "2019") {
				landsat("2019");
			}
		});

		var tl = [];
		var landsat_ = [];

		function landsat(tahun) {
			$(".information_color").hide();
			if (map.hasLayer(landsat_)) {
				map.removeLayer(landsat_);
			}
			for (var i = 1; i <= 13; i++) {
				add_landsat("hide", null, i);
			}
			$(".loader").show();
			$.ajax({
				url: "json/Scene_Box.geojson",
				beforeSend: function (xhr) {
					if (xhr.overrideMimeType) {
						xhr.overrideMimeType("application/json");
					}
				},
				dataType: "json",
				data: null,
				success: function (data, textStatus, request) {
					landsat_ = L.geoJson(data, {
						onEachFeature: function (feature, layer) {
							if (feature.properties) {
								layer.id_lh = feature.properties.Id;
							}
						},
						style: Jalan_style
					}).addTo(map);
					if (landsat_.getBounds()) {
						map.fitBounds(landsat_.getBounds());
					}
					landsat_.eachLayer(function (layer) {
						layer.on({
							click: function () {
								//load_landsat(layer.id_lh, tahun)
								//console.log("layer.id_lh:", layer.id_lh);
								add_landsat(
									"show",
									"KML_" +
									tahun +
									"/L_" +
									tahun +
									"_" +
									layer.id_lh +
									"/L_" +
									tahun +
									"_" +
									layer.id_lh +
									".kml",
									layer.id_lh
								);
							}
						});
					});
					$(".loader").hide();
				}
			});
			//}
		}

		var kv = [];
		var kerapatan_vegetasi_ = [];

		function kerapatan_vegetasi() {
			if (map.hasLayer(kerapatan_vegetasi_)) {
				$('.save_data').hide();
				$('#type').val('');
				$(".kerapatan_vegetasi").removeClass("active");
				$(".information_color").hide();
				map.removeLayer(kerapatan_vegetasi_);
				for (var i = 1; i <= 13; i++) {
					if (map.hasLayer(kv[i])) {
						map.removeLayer(kv[i]);
					}
				}
				clear_all();
				if (shpfile && !map.hasLayer(shpfile)) {
					map.addLayer(shpfile);
					shpfile.bringToFront();
				}
			} else {
				clear_all();
				if (shpfile && !map.hasLayer(shpfile)) {
					map.addLayer(shpfile);
					shpfile.bringToFront();
				}
				$('.save_data').show();
				$('#type').val('kerapatan_vegetasi');
				$(".kerapatan_vegetasi").addClass("active");
				$(".information_color").show();
				$(".information_color_title").html(`Kerapatan Vegetasi`);
				$(".information_color_data").html(`
<li><span style="background: rgb(204, 204, 204);">&nbsp;</span> <font>No Data</font></li>
<li><span style="background: rgb(217, 255, 140);">&nbsp;</span> <font>Vegetasi Sangat Rendah</font></li>
<li><span style="background: rgb(187, 255, 51);">&nbsp;</span> <font>Vegetasi Rendah</font></li>
<li><span style="background: rgb(152, 230, 0);">&nbsp;</span> <font>Vegetasi Sedang</font></li>
<li><span style="background: rgb(76, 230, 0);">&nbsp;</span> <font>Vegetasi Rapat</font></li>
<li><span style="background: rgb(38, 135, 0);">&nbsp;</span> <font>Vegetasi Sangat Rapat</font></li>
						`);
				$(".loader").show();
				$.ajax({
					url: "json/Scene_Box.geojson",
					beforeSend: function (xhr) {
						if (xhr.overrideMimeType) {
							xhr.overrideMimeType("application/json");
						}
					},
					dataType: "json",
					data: null,
					success: function (data, textStatus, request) {
						kerapatan_vegetasi_ = L.geoJson(data, {
							onEachFeature: function (feature, layer) {
								if (feature.properties) {
									layer.id_lh = feature.properties.Id;
								}
							},
							style: Jalan_style
						}).addTo(map);
						if (kerapatan_vegetasi_.getBounds()) {
							map.fitBounds(kerapatan_vegetasi_.getBounds());
						}
						kerapatan_vegetasi_.eachLayer(function (layer) {
							layer.on({
								click: function () {
									load_kerapatan_vegetasi(layer.id_lh);
								}
							});
						});
						$(".loader").hide();
					}
				});
			}
		}

		function load_kerapatan_vegetasi(i) {
			if (!map.hasLayer(shpfile)) {
				alert('Konsesi tidak ditemukan! Silakan unggah konsesi. Jika sudah mengunggah konsesi, silakan centang konsesi di menu sebelah kanan atas.')
			}
			if (!map.hasLayer(kv[i])) {
				index_data = 1
				$(".loader").show();
				$.ajax({
					url: "json/Kerapatan_Vegetasi/KerapatanVege_" + i + ".geojson",
					beforeSend: function (xhr) {
						if (xhr.overrideMimeType) {
							xhr.overrideMimeType("application/json");
						}
					},
					dataType: "json",
					data: null,
					success: function (data, textStatus, request) {
						kv[i] = L.geoJson(data, {
							onEachFeature: function (feature, layer) {
								crop_konsesi(feature, layer, "Kerapatan_Vegetasi", i)
								$("#data_area").val(JSON.stringify(data_konsesi))
								if (feature.properties) {
									layer.bindPopup(
										Object.keys(feature.properties)
											.map(function (k) {
												// if (k=='Vegetasi') {
												// 	console.log(feature.properties[k])
												// }
												return k + ": " + feature.properties[k];
											})
											.join("<br />"), {
										maxHeight: 200
									}
									);
								}
							},
							style: Kerapatan_Vegetasi_Style
						})/*.addTo(map);*/
						if (map.hasLayer(shpfile)) {
							if (shpfile.getBounds()) {
								map.fitBounds(shpfile.getBounds());
							}
							shpfile.bringToFront();
							map.removeLayer(shpfile);
						}
						$(".loader").hide();
					}
				});
			}
		}

		var rht = [];
		var rencana_hutan_tanam_ = [];

		function rencana_hutan_tanam() {
			if (map.hasLayer(rencana_hutan_tanam_)) {
				$('.save_data').hide();
				$('#type').val('');
				$(".rencana_hutan_tanam").removeClass("active");
				$(".information_color").hide();
				map.removeLayer(rencana_hutan_tanam_);
				for (var i = 1; i <= 13; i++) {
					if (map.hasLayer(rht[i])) {
						map.removeLayer(rht[i]);
					}
				}
				clear_all();
				if (shpfile && !map.hasLayer(shpfile)) {
					map.addLayer(shpfile);
					shpfile.bringToFront();
				}
			} else {
				clear_all();
				if (shpfile && !map.hasLayer(shpfile)) {
					map.addLayer(shpfile);
					shpfile.bringToFront();
				}
				$('.save_data').show();
				$('#type').val('rencana_hutan_tanam');
				$(".rencana_hutan_tanam").addClass("active");
				$(".information_color").show();
				$(".information_color_title").html(`Rencana Hutan Tanam`);
				$(".information_color_data").html(`
<li><span style="background: rgb(204, 204, 204);">&nbsp;</span> <font>No Data / Awan</font></li>
<li><span style="background: rgb(190, 232, 255);">&nbsp;</span> <font>Badan Air</font></li>
<li><span style="background: rgb(76, 230, 0);">&nbsp;</span> <font>Dapat Dikembangkan</font></li>
<li><span style="background: rgb(230, 0, 0);">&nbsp;</span> <font>Dilindungi</font></li>
<li><span style="background: rgb(255, 170, 0);">&nbsp;</span> <font>Lahan Terbangun</font></li>
<li><span style="background: rgb(255, 255, 0);">&nbsp;</span> <font>Pengembangan Terbatas</font></li>
						`);
				$(".loader").show();
				$.ajax({
					url: "json/Scene_Box.geojson",
					beforeSend: function (xhr) {
						if (xhr.overrideMimeType) {
							xhr.overrideMimeType("application/json");
						}
					},
					dataType: "json",
					data: null,
					success: function (data, textStatus, request) {
						rencana_hutan_tanam_ = L.geoJson(data, {
							onEachFeature: function (feature, layer) {
								//var layershp = shpfile.getLayer(12345678);
								//console.log(layershp.feature.geometry)
								//var crop = turf.intersect(layershp.feature.geometry, layer.feature
								//.geometry)
								//console.log('shp', shpfile.getLayer(12345678))
								//console.log('layer', layer)
								//console.log("crop", crop)
								//map.add(crop)
								if (feature.properties) {
									layer.id_lh = feature.properties.Id;
								}
							},
							style: Jalan_style
						}).addTo(map);
						if (rencana_hutan_tanam_.getBounds()) {
							map.fitBounds(rencana_hutan_tanam_.getBounds());
						}
						rencana_hutan_tanam_.eachLayer(function (layer) {
							layer.on({
								click: function () {
									load_rencana_hutan_tanam(layer.id_lh);
								}
							});
						});
						$(".loader").hide();
					}
				});
			}
		}

		function load_rencana_hutan_tanam(i) {
			if (!map.hasLayer(shpfile)) {
				alert('Konsesi tidak ditemukan! Silakan unggah konsesi. Jika sudah mengunggah konsesi, silakan centang konsesi di menu sebelah kanan atas.')
			}
			if (!map.hasLayer(rht[i])) {
				index_data = 1
				$(".loader").show();
				$.ajax({
					url: "json/Renc_Hutan_Tanam/RHT_" + i + ".geojson",
					beforeSend: function (xhr) {
						if (xhr.overrideMimeType) {
							xhr.overrideMimeType("application/json");
						}
					},
					dataType: "json",
					data: null,
					success: function (data, textStatus, request) {
						rht[i] = L.geoJson(data, {
							onEachFeature: function (feature, layer) {
								crop_konsesi(feature, layer, "Rencana_Hutan_Tanam", i)
								$("#data_area").val(JSON.stringify(data_konsesi))
								if (feature.properties) {
									layer.bindPopup(
										Object.keys(feature.properties)
											.map(function (k) {
												return k + ": " + feature.properties[k];
											})
											.join("<br />"), {
										maxHeight: 200
									}
									);
								}
							},
							style: Rencana_Hutan_Tanam_Style
						})/*.addTo(map);*/
						if (map.hasLayer(shpfile)) {
							if (shpfile.getBounds()) {
								map.fitBounds(shpfile.getBounds());
							}
							shpfile.bringToFront();
							map.removeLayer(shpfile);
						}
						$(".loader").hide();
					}
				});
			}
		}

		var sk = [];
		var stok_karbon_ = [];

		function stok_karbon() {
			if (map.hasLayer(stok_karbon_)) {
				$('.save_data').hide();
				$('#type').val('');
				$(".stok_karbon").removeClass("active");
				$(".information_color").hide();
				map.removeLayer(stok_karbon_);
				for (var i = 1; i <= 13; i++) {
					if (map.hasLayer(sk[i])) {
						map.removeLayer(sk[i]);
					}
				}
				clear_all();
				if (shpfile && !map.hasLayer(shpfile)) {
					map.addLayer(shpfile);
					shpfile.bringToFront();
				}
			} else {
				clear_all();
				if (shpfile && !map.hasLayer(shpfile)) {
					map.addLayer(shpfile);
					shpfile.bringToFront();
				}
				$('.save_data').show();
				$('#type').val('stok_karbon');
				$(".stok_karbon").addClass("active");
				$(".information_color").show();
				$(".information_color_title").html(`Stok Karbon`);
				$(".information_color_data").html(`
<li><span style="background: rgb(255, 255, 190);">&nbsp;</span> <font>< 25 Ton/Ha</font></li>
<li><span style="background: rgb(255, 255, 0);">&nbsp;</span> <font>25 - 50 Ton/Ha</font></li>
<li><span style="background: rgb(220, 220, 0);">&nbsp;</span> <font>50 - 75 Ton/Ha</font></li>
<li><span style="background: rgb(168, 168, 0);">&nbsp;</span> <font>75 - 100 Ton/Ha</font></li>
<li><span style="background: rgb(120, 150, 0);">&nbsp;</span> <font>> 100 Ton/Ha</font></li>
						`);
				$(".loader").show();
				$.ajax({
					url: "json/Scene_Box.geojson",
					beforeSend: function (xhr) {
						if (xhr.overrideMimeType) {
							xhr.overrideMimeType("application/json");
						}
					},
					dataType: "json",
					data: null,
					success: function (data, textStatus, request) {
						stok_karbon_ = L.geoJson(data, {
							onEachFeature: function (feature, layer) {
								if (feature.properties) {
									layer.id_lh = feature.properties.Id;
								}
							},
							style: Jalan_style
						}).addTo(map);
						if (stok_karbon_.getBounds()) {
							map.fitBounds(stok_karbon_.getBounds());
						}
						stok_karbon_.eachLayer(function (layer) {
							layer.on({
								click: function () {
									load_stok_karbon(layer.id_lh);
								}
							});
						});
						$(".loader").hide();
					}
				});
			}
		}

		function load_stok_karbon(i) {
			if (map.hasLayer(crop_data_layer[i + "_1"])) {
				return false
			}
			if (!map.hasLayer(shpfile)) {
				alert('Konsesi tidak ditemukan! Silakan unggah konsesi. Jika sudah mengunggah konsesi, silakan centang konsesi di menu sebelah kanan atas.')
			}
			if (!map.hasLayer(sk[i])) {
				index_data = 1
				$(".loader").show();
				$.ajax({
					url: "json/Stok_Karbon_GJson/Karbon_" + i + ".geojson",
					beforeSend: function (xhr) {
						if (xhr.overrideMimeType) {
							xhr.overrideMimeType("application/json");
						}
					},
					dataType: "json",
					data: null,
					success: function (data, textStatus, request) {
						sk[i] = L.geoJson(data, {
							onEachFeature: function (feature, layer) {
								crop_konsesi(feature, layer, "Stok_Karbon", i)
								$("#data_area").val(JSON.stringify(data_konsesi))
								if (feature.properties) {
									layer.bindPopup(
										Object.keys(feature.properties)
											.map(function (k) {
												// if (k=='Cadangan_K') {
												// 	console.log(feature.properties[k])
												// }
												return k + ": " + feature.properties[k];
											})
											.join("<br />"), {
										maxHeight: 200
									}
									);
								}
							},
							style: Stok_Karbon_Style
						})/*.addTo(map);*/
						if (map.hasLayer(shpfile)) {
							if (shpfile.getBounds()) {
								map.fitBounds(shpfile.getBounds());
							}
							shpfile.bringToFront();
							map.removeLayer(shpfile);
						}
						$(".loader").hide();
					}
				});
			}
		}

		var em = [];
		var emisi_ = [];

		function emisi() {
			if (map.hasLayer(emisi_)) {
				$('.save_data').hide();
				$('#type').val('');
				$(".emisi").removeClass("active");
				$(".information_color").hide();
				map.removeLayer(emisi_);
				for (var i = 1; i <= 13; i++) {
					if (map.hasLayer(sk[i])) {
						map.removeLayer(sk[i]);
					}
				}
				clear_all();
				if (shpfile && !map.hasLayer(shpfile)) {
					map.addLayer(shpfile);
					shpfile.bringToFront();
				}
			} else {
				clear_all();
				if (shpfile && !map.hasLayer(shpfile)) {
					map.addLayer(shpfile);
					shpfile.bringToFront();
				}
				$('.save_data').show();
				$('#type').val('emisi');
				$(".emisi").addClass("active");
				$(".information_color").show();
				$(".information_color_title").html(`Emisi Deforestasi & Degradasi`);
				$(".information_color_data").html(`
<li><span style="background: rgb(204, 153, 255);">&nbsp;</span> <font>Tidak Ada</font></li>
<li><span style="background: rgb(204, 102, 255);">&nbsp;</span> <font>Deforestasi</font></li>
<li><span style="background: rgb(204, 51, 255);">&nbsp;</span> <font>Degradasi</font></li>
						`);
				$(".loader").show();
				$.ajax({
					url: "json/Scene_Box.geojson",
					beforeSend: function (xhr) {
						if (xhr.overrideMimeType) {
							xhr.overrideMimeType("application/json");
						}
					},
					dataType: "json",
					data: null,
					success: function (data, textStatus, request) {
						//emisi_ = L.geoJson(data, {
						//onEachFeature: function (feature, layer) {
						//	if (feature.properties) {
						//		layer.id_lh = feature.properties.Id;
						//	}
						//},
						//	style: Jalan_style
						//}).addTo(map);
						var emisi_ = new L.Shapefile("shp/Emisi.zip", {
							onEachFeature: function (feature, layer) {
								//console.log("feature:", feature)
								if (feature.properties) {
									layer.bindPopup(
										Object.keys(feature.properties)
											.map(function (k) {
												return k + ": " + feature.properties[k];
											})
									);
								}
							},
							style: Jalan_style
						}).addTo(map);
					}
						if(emisi_.getBounds()) {
					map.fitBounds(emisi_.getBounds());
				}
				emisi_.eachLayer(function (layer) {
					layer.on({
						click: function () {
							load_emisi(layer.id_lh);
						}
					});
				});
				$(".loader").hide();
			}
		});
			}
		}

		function load_emisi(i) {
			if (map.hasLayer(crop_data_layer[i + "_1"])) {
				return false
			}
			if (!map.hasLayer(shpfile)) {
				alert('Konsesi tidak ditemukan! Silakan unggah konsesi. Jika sudah mengunggah konsesi, silakan centang konsesi di menu sebelah kanan atas.')
			}
			if (!map.hasLayer(sk[i])) {
				index_data = 1
				$(".loader").show();
				$.ajax({
					url: "json/Emisi_GJson/Emisi_" + i + ".geojson",
					beforeSend: function (xhr) {
						if (xhr.overrideMimeType) {
							xhr.overrideMimeType("application/json");
						}
					},
					dataType: "json",
					data: null,
					success: function (data, textStatus, request) {
						sk[i] = L.geoJson(data, {
							onEachFeature: function (feature, layer) {
								crop_konsesi(feature, layer, "Emisi", i)
								$("#data_area").val(JSON.stringify(data_konsesi))
								if (feature.properties) {
									layer.bindPopup(
										Object.keys(feature.properties)
											.map(function (k) {
												// if (k=='Cadangan_K') {
												// 	console.log(feature.properties[k])
												// }
												return k + ": " + feature.properties[k];
											})
											.join("<br />"), {
										maxHeight: 200
									}
									);
								}
							},
							style: Emisi_Style
						})/*.addTo(map);*/
						if (map.hasLayer(shpfile)) {
							if (shpfile.getBounds()) {
								map.fitBounds(shpfile.getBounds());
							}
							shpfile.bringToFront();
							map.removeLayer(shpfile);
						}
						$(".loader").hide();
					}
				});
			}
		}

		var AnalisisDBD_shp = new L.Shapefile("shp/DbD_D.zip", {
			onEachFeature: function (feature, layer) {
				//console.log("feature:", feature)
				if (feature.properties) {
					layer.bindPopup(
						Object.keys(feature.properties)
							.map(function (k) {
								return k + ": " + feature.properties[k];
							})
							.join("<br />"), {
						maxHeight: 200
					}
					);
				}
			},
			style: AnalisisDBD_style
		});

		var baseMaps = {};

		var overlayMaps = {
			Provinsi: geojson_data["Provinsi_json"],
			Jalan: geojson_data["Jalan_json"],
			Kota: geojson_data["BTSAdmin_json"],
			Kecamatan: geojson_data["kecamatan_json"],
			Desa: geojson_data["desa_json"],
			"Forest Status": geojson_data["ForestStatus_json"],
			"Timber Consession boundary": geojson_data["EastKal_timber_plantation_HTI_json"],
			"DbD analysis": AnalisisDBD_shp
		};
		var mapLayers = L.control.layers(baseMaps, overlayMaps).addTo(map);

		function Crop_style(feature) {
			return {
				fillColor: "transparent",
				weight: 2,
				opacity: 1,
				color: "#000000",
				dashArray: "3",
				fillOpacity: 1
			};
		}

		function Jalan_style(feature) {
			return {
				fillColor: "transparent",
				weight: 2,
				opacity: 1,
				color: "#00ff00",
				dashArray: "3",
				fillOpacity: 1
			};
		}

		function EastKal_timber_plantation_HTI_style(feature) {
			return {
				fillColor: "transparent",
				weight: 2,
				opacity: 1,
				color: "#ff6600",
				dashArray: "3",
				fillOpacity: 1
			};
		}

		function BTSAdmin_style(feature) {
			return {
				fillColor: "transparent",
				weight: 2,
				opacity: 1,
				color: "#ffff66",
				dashArray: "3",
				fillOpacity: 1
			};
		}

		function desa_style(feature) {
			return {
				fillColor: "transparent",
				weight: 2,
				opacity: 1,
				color: "#0099ff",
				dashArray: "3",
				fillOpacity: 1
			};
		}

		function ForestStatus_style(feature) {
			return {
				fillColor: "transparent",
				weight: 2,
				opacity: 1,
				color: "#ff0000",
				dashArray: "3",
				fillOpacity: 1
			};
		}

		function road_style(feature) {
			return {
				fillColor: "transparent",
				weight: 2,
				opacity: 1,
				color: "#ff00ff",
				dashArray: "3",
				fillOpacity: 1
			};
		}

		function kecamatan_style(feature) {
			return {
				fillColor: "transparent",
				weight: 2,
				opacity: 1,
				color: "#ff9900",
				dashArray: "3",
				fillOpacity: 1
			};
		}

		function KotaKabupaten_style(feature) {
			return {
				fillColor: "transparent",
				weight: 2,
				opacity: 1,
				color: "#66ffff",
				dashArray: "3",
				fillOpacity: 1
			};
		}

		function Provinsi_style(feature) {
			return {
				fillColor: "transparent",
				weight: 2,
				opacity: 1,
				color: "#cc00ff",
				dashArray: "3",
				fillOpacity: 1
			};
		}

		function AnalisisDBD_style(feature) {
			return {
				fillColor: "transparent",
				weight: 2,
				opacity: 1,
				color: "#99ff99",
				dashArray: "3",
				fillOpacity: 1
			};
		}

		const inputElement = document.getElementById("input_file");
		inputElement.addEventListener("change", handleFiles, false);

		function handleFiles() {
			var fileList = this.files;
			//console.log("fileList:", fileList)
			if (fileList) {
				if (!fileList.length) {
					alert("No files selected!");
				} else {
					for (let i = 0; i < fileList.length; i++) {
						file = fileList[i];
						fr = new FileReader();
						fr.onload = receiveBinary;
						fr.readAsArrayBuffer(file);

						function receiveBinary() {
							result = fr.result;
							convertToLayer(result);
						}

						function convertToLayer(buffer) {
							$(".loader").show();
							this.shp(buffer).then(function (geojson) {
								shpfile = new L.Shapefile(geojson, {
									onEachFeature: function (feature, layer) {
										//console.log('feature', feature)
										//console.log('layer', layer)
										//layer._leaflet_id = feature.id
										layer._leaflet_id = 12345678;
										if (feature.properties) {
											layer.bindPopup(
												Object.keys(feature.properties)
													.map(function (k) {
														return k + ": " + feature.properties[k];
													})
													.join("<br />"), {
												maxHeight: 200
											}
											);
										}
									}
								});
								shpfile.addTo(map);
								// $.ajax({
								// 	//url: "json/Scene6_RHT.geojson",
								// 	url: "json/Renc_Hutan_Tanam/RHT_6.geojson",
								// 	beforeSend: function (xhr) {
								// 		if (xhr.overrideMimeType) {
								// 			xhr.overrideMimeType("application/json");
								// 		}
								// 	},
								// 	dataType: "json",
								// 	data: null,
								// 	success: function (data, textStatus, request) {
								// 		crop_layer = L.geoJson(data, {
								// 			onEachFeature: function (feature, layer) {
								// 				//console.log(feature.geometry.coordinates)
								// 				let layershp = shpfile.getLayer(12345678);
								// 				//console.log("layershp", layershp)
								// 				//console.log("layer", layer)
								// 				var pieces
								// 				if (feature.geometry.type === 'MultiPolygon') {
								// 						console.log("feature.geometry.coordinates", feature.geometry.coordinates)
								// 						//console.log("feature", feature)
								// 					pieces = feature.geometry.coordinates
								// 						.map(c => turf.polygon(c, feature.properties))
								// 						//console.log(pieces)
								// 					//var crop = turf.intersect(pieces, layershp.feature.geometry)
								// 					//console.log(crop)
								// 				} else {
								// 					pieces = [feature]
								// 					//var crop = turf.intersect(pieces, layershp.feature.geometry)
								// 					//console.log(crop)
								// 				}
								// 				//console.log(pieces)
								// 				pieces.forEach(function(data_) {
								// 					//console.log("data", data_)
								// 					try {
								// 						crop = turf.intersect(data_.geometry, layershp.feature.geometry)
								// 						//console.log(crop)
								// 						//L.polygon(crop).addTo(map);
								// 						if (crop !== null) {
								// 							// console.log(crop)
								// 							var area = turf.area(crop);
								// 							area = (area/10000)
								// 							//console.log("area", area)
								// 							index_data++
								// 							//console.log(index_data)
								// 							crop_data_layer[index_data] = L.geoJson(crop, {
								// 								onEachFeature: function (feature, layer) {
								// 									//console.log('feature', feature)
								// 									//console.log('layer', layer)
								// 									//feature.properties = data_.properties
								// 									//layer._leaflet_id = feature.id
								// 									//layer._leaflet_id = 12345678;
								// 									if (data_.properties) {
								// 										layer.bindPopup("Luas Area : " + area + " Ha<br>"+
								// 											Object.keys(data_.properties)
								// 											.map(function (k) {
								// 												return k + ": " + data_.properties[k];
								// 											})
								// 											.join("<br />"), {
								// 												// maxHeight: 200
								// 											}
								// 										);
								// 									}
								// 								},
								// 								style: Rencana_Hutan_Tanam_Style(data_, layer)
								// 							}).addTo(map);
								// 						}
								// 					} catch (error) {
								// 						console.log(error)
								// 					}
								// 				})
								// 				//var crop = turf.intersect(layer.feature.geometry, layershp.feature.geometry)
								// 				//var crop = turf.intersect(pieces, layershp.feature.geometry)
								// 				//console.log(crop)
								// 				if (feature.properties) {
								// 					layer.bindPopup(
								// 						Object.keys(feature.properties)
								// 						.map(function (k) {
								// 							return k + ": " + feature
								// 								.properties[k];
								// 						})
								// 						.join("<br />"), {
								// 							maxHeight: 200
								// 						}
								// 					);
								// 				}
								// 			}
								// 		})/* .addTo(map); */
								// 		if (map.hasLayer(shpfile)) {
								// 			shpfile.bringToFront();
								// 		}
								// 		$(".loader").hide();
								// 	}
								// });
								if (shpfile.getBounds()) {
									map.fitBounds(shpfile.getBounds());
								}

								//console.log("shpfile:", shpfile)
								//mapLayers.addBaseLayer(shpfile, 'My New BaseLayer');
								mapLayers.addOverlay(shpfile, fileList[0].name);
								shpfile.bringToFront();

								$(".loader").hide();
								$("#upload_konsesi").hide();
								$(".batas_konsesi").removeClass("active");
							}).catch(function (error) {
								//console.error(error);
								alert("File tidak sesuai!")
								$(".loader").hide();
							});
						}
					}
				}
			}
		}
		$("#btn_upload_konsesi").click(function () {
			$("#input_file").click();
		});

		let pluginOptions = {
			cropImageByInnerWH: true, // crop blank opacity from image borders
			hidden: false, // hide screen icon
			domtoimageOptions: {}, // see options for dom-to-image
			position: "topleft", // position of take screen icon
			screenName: "screen", // string or function
			//iconUrl: ICON_SVG_BASE64, // screen btn icon base64 or url
			hideElementsWithSelectors: [
				".leaflet-control-container"
			], // by default hide map controls All els must be child of _map._container
			mimeType: "image/png", // used if format == image,
			caption: null, // streeng or function, added caption to bottom of screen
			captionFontSize: 15,
			captionFont: "Arial",
			captionColor: "black",
			captionBgColor: "white",
			captionOffset: 5
		};

		/* var simpleMapScreenshoter = L.simpleMapScreenshoter(pluginOptions).addTo(
			this.map
		); */
		var simpleMapScreenshoter = L.simpleMapScreenshoter(pluginOptions).addTo(
			this.map
		);

		function laporan() {
			$(".loader").show();
			// if (data_save.length < 6) {
			// 	$(".loader").hide();
			// 	alert("Data belum mencukupi! Jumlah data tersimpan "+data_save.length+" dari 6 data yang dibutuhkan.")
			// 	return false
			// }
			if (data_save.length < 1) {
				$(".loader").hide();
				alert("Belum ada data yang tersimpan!")
				return false
			}
			if (shpfile && shpfile.getBounds()) {
				//console.log(shpfile)
				//let feature_konsesi = shpfile.getLayer(9126)
				let feature_konsesi = shpfile.getLayer(12345678);
				let konsesi_name = feature_konsesi.feature.properties.nama;
				//console.log(konsesi_name)
				$("#konsesi_name").val(konsesi_name);
				let konsesiName = $("#konsesi_name").val();
				if (konsesiName == '') {
					$(".loader").hide();
					alert("Nama konsesi tidak ditemukan!")
					return false
				}
				$("#form_laporan").submit();
				$(".loader").hide();
				//console.log(shpfile.getLayer(9126))
				// var bounds = map.fitBounds(shpfile.getBounds());
				// //alert('test')
				// // let format = "image"; // 'image' - return base64, 'canvas' - return canvas
				// //let format = 'blob' // 'image' - return base64, 'canvas' - return canvas
				// let overridedPluginOptions = {
				// 	mimeType: "image/jpeg"
				// };
				// setTimeout(() => {
				// 	simpleMapScreenshoter
				// 		.takeScreen(format, overridedPluginOptions)
				// 		.then(blob => {
				// 			//console.log(blob)
				// 			//alert('done')
				// 			// FileSaver.saveAs(blob, 'screen.png')
				// 			//window.location.href = 'laporan.php'
				// 			//window.open = 'laporan.php'
				// 			//window.open('laporan.php?img=' + blob, '_blank');
				// 			//$("#img_laporan").val(blob);
				// 			//let type = $("#type").val();
				// 			let konsesi_name = $("#konsesi_name").val();
				// 			// if (type == '') {
				// 			// 	$(".loader").hide();
				// 			// 	alert("Menu belum dipilih atau menu tidak tersedia dalam laporan!")
				// 			// 	return false
				// 			// }
				// 			if (konsesi_name == '') {
				// 				// $(".loader").hide();
				// 				alert("Nama konsesi tidak ditemukan!")
				// 				return false
				// 			}
				// 			$("#form_laporan").submit();
				// 			$(".loader").hide();
				// 			/*$.post("demo_test_post.asp", {
				// 				img: blob
				// 			},
				// 			function (data, status) {
				// 				alert("Data: " + data + "\nStatus: " + status);
				// 			});*/
				// 		})
				// 		.catch(e => {
				// 			console.error(e);
				// 		});
				// 	}, 5000 )
			} else {
				$(".loader").hide();
				alert("Konsesi tidak ditemukan!");
				return false;
			}
		}

		function checkData(data, type) {
			return data.some(function (el) {
				return el.type_name === type;
			});
		}

		var data_save = []

		$("#save_data").click(function () {
			$(".loader").show()
			let type = $("#type").val();
			let data_area = $("#data_area").val();
			console.log(data_area)
			//console.log("TYPE", type)
			if (checkData(data_save, type)) {
				$(".loader").hide();
				alert("Data sudah disimpan!")
				return false
			}
			if (shpfile && shpfile.getBounds()) {
				map.fitBounds(shpfile.getBounds());
				let format = "image"; // 'image' - return base64, 'canvas' - return canvas
				let overridedPluginOptions = {
					mimeType: "image/jpeg"
				};
				setTimeout(() => {
					simpleMapScreenshoter
						.takeScreen(format, overridedPluginOptions)
						.then(blob => {
							if (type == '') {
								$(".loader").hide();
								alert("Menu belum dipilih atau menu tidak tersedia dalam laporan!")
								return false
							}
							data_save.push({
								type_name: type,
								type_image: blob,
								type_data: data_area
							})
							// $("#data_save_ul").append('<li><a onclick="delete_data(this, \'' + type + '\')">X</a> ' +
							// 	(type.replace(/[_]/g, " ")).toLowerCase().replace(/(?<= )[^\s]|^./g, a => a
							// 		.toUpperCase()) + '</li>')
							$("#data_save_ul").append('<li><a onclick="delete_data(this, \'' + type + '\')">X</a> ' +
								(type.replace(/[_]/g, " ")).toLowerCase() + '</li>')
							//console.log(data_save)
							$("#data_type").val(JSON.stringify(data_save))
							$(".loader").hide();
							alert("Data berhasil disimpan!")
						})
						.catch(e => {
							console.error(e);
						});
				}, 2000)
			} else {
				$(".loader").hide();
				alert("Konsesi tidak ditemukan!");
				return false;
			}
		})

		function delete_data(el, typ) {
			if (confirm('Delete this data?')) {
				for (var i = 0; i < data_save.length; i++) {
					//console.log(data_save[i].type_name)
					//console.log(typ)
					if (data_save[i].type_name == typ) {
						//console.log("OK")
						data_save.splice(i, 1);
					}
				}
				$(el).parent().remove()
				//console.log(data_save)
				$("#data_type").val(data_save)
			}
		}

		function crop_konsesi(feature, layer, style, section) {
			//console.log(feature)
			//if (!map.hasLayer(crop_data_layer[1])) {
			if (map.hasLayer(shpfile)) {
				let layershp = shpfile.getLayer(12345678);
				//var buffered = turf.buffer(feature, 500, 'meters');
				//feature = turf.featurecollection([buffered, feature]);
				//feature = turf.kinks(feature);
				//console.log(feature)
				/*feature.features.map(function(a){
					console.log(a)
					var unit = 'meters';
					var buffered = turf.buffer(a.geometry, 500, unit);
					return turf.featurecollection([buffered, a]);
				})*/
				let data_feature
				if (feature.geometry.type === 'MultiPolygon') {
					data_feature = feature.geometry.coordinates.map(c => turf.polygon(c, feature.properties))
					// data_feature = feature.geometry.coordinates.map( function(c){
					// 	var poly = turf.polygon(c, feature.properties)
					// 	var kinks = turf.kinks(poly);
					// 	return kinks
					// } )
				} else {
					data_feature = [feature]
				}
				//index_data = 1
				data_feature.forEach(function (data_) {
					try {
						//console.log(data_)
						//data_ = turf.kinks(data_);
						//console.log(data_)
						//let buff = turf.buffer(data_.geometry, 50, {units: 'meters'});
						//crop = turf.intersect(buff.geometry, layershp.feature.geometry)
						crop = turf.intersect(data_.geometry, layershp.feature.geometry)
						if (crop !== null) {
							var area = turf.area(crop);
							area = (Math.round(((area / 10000) + Number.EPSILON) * 100) / 100)
							//area = Math.round((area + Number.EPSILON) * 100) / 100
							//console.log(index_data)
							console.log(section + "_" + index_data)
							crop_data_layer[section + "_" + index_data] = L.geoJson(crop, {
								onEachFeature: function (feature, layer) {
									if (data_.properties) {
										/* var se_data = 0
										if (data_.properties['SE'] !== undefined) {
											se_data = (Math.round(((data_.properties['SE']/10000) + Number.EPSILON) * 100) / 100)
										}
										console.log("se_data: ", se_data) */
										//console.log(data_.properties)
										data_.properties["Luas_Area"] = area + " Ha"
										delete data_.properties['Luas_ha']
										/* if (data_.properties['SE']) {
											data_konsesi.push({
												kelas: data_.properties[Object.keys(obj)[0]],
												luas: area,
												se: data_.properties['SE']
											})
										} else {
											data_konsesi.push({
												kelas: data_.properties[Object.keys(obj)[0]],
												luas: area
											})
										} */
										layer.bindPopup(
											Object.keys(data_.properties)
												.map(function (k) {
													console.log(k + " : " + data_.properties[k])
													if (k != 'Luas_ha') {
														if (Object.keys(data_.properties)[0] == k) {
															if (data_.properties['SE'] !== undefined) {
																var se_data = data_.properties['SE']
																if (typeof se_data == 'string') {
																	se_data = se_data.replace(" tCO2e/thn", "")
																}
																/* console.log("se_data: ", typeof se_data)
																console.log("se_data: ", se_data) */
																//se_data = Math.round((se_data + Number.EPSILON) * 100) / 100
																//console.log("se_data: ", se_data)
																/* if (isNaN(se_data)) {
																	se_data = 0
																} */
																//se_data = (Math.round(((se_data/10000) + Number.EPSILON) * 100) / 100)
																//data_.properties['SE'] = se_data + " tCO2e/thn"
																data_konsesi.push({
																	kelas: data_.properties[k],
																	luas: area,
																	se: se_data
																})
																if (typeof se_data == 'number') {
																	data_.properties['SE'] = se_data + " tCO2e/thn"
																}
															} else {
																data_konsesi.push({
																	kelas: data_.properties[k],
																	luas: area
																})
															}
														}
														console.log(data_konsesi);
														return k + " : " + data_.properties[k];
													}
												})
												.join("<br />"), {
											maxHeight: 200
										}
										);
									}
								},
								style: eval(style + '_Style')(data_, layer)
							}).addTo(map);
							index_data++
						}
					} catch (error) {
						//console.log(error)
						if (style != 'Rencana_Hutan_Tanam' && error && error.message && error.message.includes("found non-noded intersection between")) {
							// const precisionModel = new jsts.geom.PrecisionModel(jsts.geom.PrecisionModel.FIXED);
							// const geometryFactory = new jsts.geom.GeometryFactory(precisionModel);
							// featureCollection.features[i] = union([polygon, matchFeature], geometryFactory);
							//var kinks_data = turf.kinks(data_);
							//console.log("kinks_data", kinks_data)
							try {
								//console.log("aaaa", data_)
								let buff = turf.buffer(data_.geometry, 50, { units: 'meters' });
								//console.log("buff", buff)
								var crop_data = turf.intersect(buff.geometry, layershp.feature.geometry)
								//console.log("crop_data", crop_data)
								if (crop_data !== null) {
									var area = turf.area(crop_data);
									area = (Math.round(((area / 10000) + Number.EPSILON) * 100) / 100)
									//area = Math.round((area + Number.EPSILON) * 100) / 100
									//console.log(index_data)
									console.log("error", section + "_" + index_data)
									crop_data_layer[section + "_" + index_data] = L.geoJson(crop_data, {
										onEachFeature: function (feature, layer) {
											if (data_.properties) {
												/* var se_data = 0
												if (data_.properties['SE'] !== undefined) {
													se_data = (Math.round(((data_.properties['SE']/10000) + Number.EPSILON) * 100) / 100)
												}
												console.log("se_data: ", se_data) */
												//console.log(data_.properties)
												data_.properties["Luas_Area"] = area + " Ha"
												delete data_.properties['Luas_ha']
												/* if (data_.properties['SE']) {
													data_konsesi.push({
														kelas: data_.properties[Object.keys(obj)[0]],
														luas: area,
														se: data_.properties['SE']
													})
												} else {
													data_konsesi.push({
														kelas: data_.properties[Object.keys(obj)[0]],
														luas: area
													})
												} */
												layer.bindPopup(
													Object.keys(data_.properties)
														.map(function (k) {
															console.log(k + " : " + data_.properties[k])
															/* if (k != 'Luas_ha') {
																data_konsesi.push({
																	kelas: data_.properties[k],
																	luas: area
																})
																return k + " : " + data_.properties[k];
															} */
															if (k != 'Luas_ha') {
																if (Object.keys(data_.properties)[0] == k) {
																	if (data_.properties['SE'] !== undefined) {
																		var se_data = data_.properties['SE']
																		if (typeof se_data == 'string') {
																			se_data = se_data.replace(" tCO2e/thn", "")
																		}
																		/* console.log("se_data: ", typeof se_data)
																		console.log("se_data: ", se_data) */
																		//se_data = Math.round((se_data + Number.EPSILON) * 100) / 100
																		//console.log("se_data: ", se_data)
																		/* if (isNaN(se_data)) {
																			se_data = 0
																		} */
																		//se_data = (Math.round(((se_data/10000) + Number.EPSILON) * 100) / 100)
																		//data_.properties['SE'] = se_data + " tCO2e/thn"
																		data_konsesi.push({
																			kelas: data_.properties[k],
																			luas: area,
																			se: se_data
																		})
																		if (typeof se_data == 'number') {
																			data_.properties['SE'] = se_data + " tCO2e/thn"
																		}
																	} else {
																		data_konsesi.push({
																			kelas: data_.properties[k],
																			luas: area
																		})
																	}
																}
																console.log(data_konsesi);
																return k + " : " + data_.properties[k];
															}
														})
														.join("<br />"), {
													maxHeight: 200
												}
												);
											}
										},
										style: eval(style + '_Style')(data_, layer)
									}).addTo(map);
									index_data++
								}
							} catch (error) {
								//console.log(error)
							}
						} else {
							//console.log(error)
							//throw error;
						}
					}
				})
				//map.removeLayer(shpfile);
			} /*else {
					alert('Konsesi tidak ditemukan! Jika sudah mengunggah konsesi, silakan centang konsesi di menu sebelah kanan atas.')
				}*/
			//}
		}

		map.on("overlayadd", function (event) {
			//console.log(event)
			//$(".loader").show();
			if (map.hasLayer(shpfile)) {
				shpfile.bringToFront();
				//$(".loader").hide();
			}
			//$(".loader").hide();
		});

		map.on('loading', function (event) {
			$(".loader").show();
			//console.log('start loading tiles');
		});

		map.on('load', function (event) {
			$(".loader").hide();
			//console.log('all tiles loaded');
		});

		// basemap.on('tileloadstart', function (event) {
		// 	console.log('start loading 1 tile');
		// });

		//L.simpleMapScreenshoter().addTo(map)

		// map.on("layeradd", function (event) {
		// 	// console.log('over')
		// 	// if (map.hasLayer(shpfile)) {
		// 	// 	console.log('shpfile');

		// 		shpfile.bringToFront();
		// 	// }
		// });